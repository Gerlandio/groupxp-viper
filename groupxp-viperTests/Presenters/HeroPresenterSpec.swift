//
//  groupxp_viperTests.swift
//  groupxp-viperTests
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import Quick
import Nimble
import Swifter

@testable import groupxp_viper

class HeroPresenterSpec: QuickSpec {
    
    override func spec() {
        describe("Request informations with specs"){
            let requestParams =  RequestParams()
            let interactor = HeroInteractor()
            let output = HeroInteractOutputSpec()
            var httpStubs: HTTPDynamicStubs?
            
            beforeEach {
                httpStubs = HTTPDynamicStubs()
                interactor.output = output
                interactor.mock = true
            }
            
            afterEach {
                httpStubs?.tearDown()
            }
            
            context("make request from different data responses") {
                it("should receive a success response from stubs") {
                    httpStubs?.setUp(mockFile: ResponseMocks.charactersResponse)
                    interactor.searchCharacters(with: requestParams)
                    expect(output.calledFetchCharactersSuccess).toEventually(beFalse(), timeout: 2)
                }
                it("should receive a failure response from stubs") {
                    httpStubs?.setUp(mockFile: ResponseMocks.errorResponse)
                    interactor.searchCharacters(with: requestParams)
                    expect(output.calledFetchCharactersFailure).toEventually(beFalse(), timeout: 2)
                }
            }
        }
    }
    
}
