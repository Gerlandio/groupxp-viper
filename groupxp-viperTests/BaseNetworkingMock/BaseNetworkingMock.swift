//
//  BaseNetworkingMock.swift
//  groupxp-viperTests
//
//  Created by Gerlandio Da Silva Lucena on 16/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
import Swifter

struct ResponseMocks {
    static let charactersResponse = "characters_response"
    static let comicsResponse = "comics_response_success"
    static let errorResponse = "missing_parameter"
}

class HTTPDynamicStubs {
    
    var server: HttpServer?
    
    enum HTTPMethod {
        case GET
    }
    
    func setUp(mockFile: String) {
        server = HttpServer()
        setupStub(url: "/v1/public/characters", filename: mockFile)
        try? server?.start()
    }
    
    func tearDown() {
        server?.stop()
    }
    
    public func setupStub(url: String, filename: String, method: HTTPMethod = .GET) {
        let testBundle = Bundle(for: type(of: self))
        let filePath = testBundle.path(forResource: filename, ofType: "json")
        let fileUrl = URL(fileURLWithPath: filePath!)
        if let data = try? Data(contentsOf: fileUrl, options: .uncached) {
            let json = dataToJSON(data: data)
            
            let response: ((HttpRequest) -> HttpResponse) = { _ in
                return HttpResponse.ok(.json(json as AnyObject))
            }
            
            switch method  {
                case .GET : server?.GET[url] = response
            }
        }
    }
    
    func dataToJSON(data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        } catch let myJSONError {
            print(myJSONError)
        }
        return nil
    }
}
