//
//  HeroWireframeSpecs.swift
//  groupxp-viperTests
//
//  Created by Gerlandio Da Silva Lucena on 16/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
@testable import groupxp_viper

class HeroWireframeSpecs: IHeroWireframe {
    var calledPresentFavoriteController = false
    var calledCreateController = false
    
    func presentFavoriteController(from view: BaseViewController) {
        calledPresentFavoriteController = true
        _ = createController()
    }
    
    func createController() -> UIViewController {
        calledCreateController = true
        return UIViewController()
    }
    

}
