//
//  HeroInteractOutputSpec.swift
//  groupxp-viperTests
//
//  Created by Gerlandio Da Silva Lucena on 16/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
@testable import groupxp_viper

class HeroInteractOutputSpec: HeroInteractOutput {
    var calledFetchCharactersSuccess = false
    var calledFetchCharactersFailure = false
    var calledPersistCharactersFailure = false
    
    func onFetchCharactersSuccess(characters: CharactersResult?, shouldAppend: Bool) {
        calledFetchCharactersSuccess = true
    }
    
    func onFetchCharactersFailure(message: String) {
        calledFetchCharactersFailure = true
    }
    
    func onPersistCharactersFailure(message: String) {
        calledPersistCharactersFailure = true
    }
    

}
