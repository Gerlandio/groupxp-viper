//
//  UITableView.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

extension UITableView {
    func dequeue<T: ReusableView>(indexPath: IndexPath) -> T? {
        return self.dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as? T
    }
}
