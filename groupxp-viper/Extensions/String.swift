//
//  Swift+Extensions.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 14/10/2018.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, comment: "")
    }
}
