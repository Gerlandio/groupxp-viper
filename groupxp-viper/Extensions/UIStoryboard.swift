//
//  UIStoryboard.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

extension UIStoryboard {
    func instantiate<T: ReusableView>() -> T?  {
        return instantiateViewController(withIdentifier: T.reuseIdentifier) as? T
    }
}
