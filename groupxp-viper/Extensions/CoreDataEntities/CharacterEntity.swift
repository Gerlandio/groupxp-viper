//
//  CharacterEntity.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 02/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

extension CharacterCD: Encodable {

    enum CodingKeys: String, CodingKey {
        case comics
        case series
        case modified
        case id
        case name
        case thumbnail
        case urls
        case descriptionValue
        case stories
        case resourceURI
        case events
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(comics, forKey: .comics)
        try container.encode(series, forKey: .series)
        try container.encode(modified, forKey: .modified)
        try container.encode(id, forKey: .id)
        try container.encode(name, forKey: .name)
        try container.encode(thumbnail, forKey: .thumbnail)
        try container.encode(descriptionValue, forKey: .descriptionValue)
        try container.encode(stories, forKey: .stories)
        try container.encode(resourceURI, forKey: .resourceURI)
        try container.encode(events, forKey: .events)
    }
}
