//
//  ComicsEntity.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 02/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

extension ComicCD: Encodable {

    enum CodingKeys: String, CodingKey {
        case items
        case available
        case returned
        case collectionURI
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        //try container.encode(items, forKey: .items)
        try container.encode(available, forKey: .available)
        try container.encode(returned, forKey: .returned)
        try container.encode(collectionURI, forKey: .collectionURI)
    }
}
