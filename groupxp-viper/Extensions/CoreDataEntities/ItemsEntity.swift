//
//  ItemsEntity.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 02/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

extension ItemsEntity: Encodable {
    
    enum CodingKeys: String, CodingKey {
        case name
        case resourceURI
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(name, forKey: .name)
        try container.encode(resourceURI, forKey: .resourceURI)
    }
}

extension ThumbnailCD: Encodable {
    enum CodingKeys: String, CodingKey {
        case extensionName = "extension"
        case path
    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        try container.encode(extensionName, forKey: .extensionName)
        try container.encode(path, forKey: .path)
    }
}
