//
//  BaseViewProtocol.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 05/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol BaseViewProtocol: class {
    func reloadInformation(with message: String)
    func setViewState(state: LoadingState)
}
