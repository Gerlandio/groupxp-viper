//
//  Data.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct DetailResultInformation: Codable {
    // MARK: Properties
    var offset: Int?
    var total: Int?
    var limit: Int?
    var results: [Detail]?
    var count: Int?
    
    enum CodingKeys: String, CodingKey {
        case offset
        case total
        case limit
        case results
        case count
    }
}
