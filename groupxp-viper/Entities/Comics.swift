//
//  Comics.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Comics: Codable {

    // MARK: Properties
    var items: [Items]?
    var available: Int?
    var returned: Int?
    var collectionURI: String?

    enum CodingKeys: String, CodingKey {
        case items
        case available
        case returned
        case collectionURI
    }
}
