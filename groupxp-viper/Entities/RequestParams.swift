//
//  RequestParams.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class RequestParams: NSObject {

    struct Constants {
        static let timeStamp = "ts"
        static let apikey = "apikey"
        static let hash = "hash"
        static let orderBy = "orderBy"
        static let limit = "limit"
        static let offset = "offset"
    }
    
    let timestamp = 26101986
    let publicKey = "70af54d16218dface286b1d445ccba84"
    let hashKey = "9badb2511cdd64e1bf76a84705e3e567"
    let orderBy = "name"
    var limit = 20
    var offset = 0
    
    func nextPage() {
        offset = offset + limit
    }
    
    func resetPage() {
        offset = 0
    }
    
    var queryString: String {
        var finalRequest: [String] = [String]()
        finalRequest.append("\(Constants.timeStamp)=\(timestamp)")
        finalRequest.append("\(Constants.apikey)=\(publicKey)")
        finalRequest.append("\(Constants.hash)=\(hashKey)")
        finalRequest.append("\(Constants.orderBy)=\(orderBy)")
        finalRequest.append("\(Constants.limit)=\(limit)")
        finalRequest.append("\(Constants.offset)=\(offset)")
        return finalRequest.joined(separator: "&")
    }
    
    var baseQueryString: String {
        var finalRequest: [String] = [String]()
        finalRequest.append("\(Constants.timeStamp)=\(timestamp)")
        finalRequest.append("\(Constants.apikey)=\(publicKey)")
        finalRequest.append("\(Constants.hash)=\(hashKey)")
        finalRequest.append("\(Constants.limit)=\(limit)")
        finalRequest.append("\(Constants.offset)=\(offset)")
        return finalRequest.joined(separator: "&")
    }
}
