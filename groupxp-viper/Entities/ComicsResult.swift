//
//  BaseClass.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct ComicsResult: Codable {
    
    // MARK: Properties
    var copyright: String?
    var status: String?
    var data: DetailResultInformation?
    var attributionText: String?
    var code: Int?
    var etag: String?
    var attributionHTML: String?

    enum CodingKeys: String, CodingKey {
        case copyright
        case status
        case data
        case attributionText
        case code
        case etag
        case attributionHTML
    }
}
