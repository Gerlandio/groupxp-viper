//
//  Results.swift
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Detail: Codable {
    var id: Int?
    var thumbnail: Thumbnail?
    var descriptionValue: String?
    var images: [Thumbnail]?
    var title: String?
    var pageCount: Int?
    var digitalId: Int?
    var resourceURI: String?
    
    enum CodingKeys: String, CodingKey {
        case id
        case thumbnail
        case descriptionValue = "description"
        case images
        case title
        case pageCount
        case digitalId
        case resourceURI
    }
}
