//
//  Urls.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Urls: Codable {

    // MARK: Properties
    public var type: String?
    public var url: String?
    
    enum CodingKeys: String, CodingKey {
        case type = "type"
        case url = "url"
    }
}
