//
//  Items.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Items: Codable {

    // MARK: Properties
    var name: String?
    var resourceURI: String?

    enum CodingKeys: String, CodingKey {
        case name
        case resourceURI
    }
}
