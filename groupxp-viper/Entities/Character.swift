//
//  Results.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

class Character: Codable {
    
    // MARK: Properties
    var comics: Comics?
    var series: Comics?
    var modified: String?
    var id: Int?
    var name: String?
    var thumbnail: Thumbnail?
    var urls: [Urls]?
    var descriptionValue: String?
    var stories: Comics?
    var resourceURI: String?
    var events: Comics?
    var favorited: Bool?
    
    enum CodingKeys: String, CodingKey {
        case comics
        case series
        case modified
        case id
        case name
        case thumbnail
        case urls
        case descriptionValue = "description"
        case stories
        case resourceURI
        case events
    }
}
