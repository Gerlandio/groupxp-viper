//
//  Thumbnail.swift
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18
//  Copyright (c) . All rights reserved.
//

import Foundation

struct Thumbnail: Codable {

    // MARK: Properties
    var extensionName: String?
    var path: String?
    
    enum CodingKeys: String, CodingKey {
        case extensionName = "extension"
        case path
    }
    
    var url: String? {
        guard let path = path, let extensionName = extensionName else {
            return nil
        }
        return "\(path).\(extensionName)"
    }
}
