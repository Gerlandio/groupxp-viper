//
//  CharacterDetailsPresenter.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright (c) 2018 Gerlandio Da Silva Lucena. All rights reserved.
//

import UIKit

protocol ICharacterDetailsPresenter: BasePresenter {
}

class CharacterDetailsPresenter: ICharacterDetailsPresenter {
	
	var interactor: ICharacterDetailsInteractor?
	weak var view: ICharacterDetailsViewController?
	var character: Character?
    var requestParams = RequestParams()
    var comicsList = [String: String]()
    var characterDetails = [String: [Detail]]()
	
    public init(interactor: ICharacterDetailsInteractor, view: ICharacterDetailsViewController, characterItem: Character?) {
		self.interactor = interactor
		self.view = view
        self.character = characterItem
	}
    
    func viewLoad() {
        loadCharacters()
    }
    
    func loadCharacters() {
        guard let character = character else {
            return
        }
        
        comicsInformation(character: character)
    }
    
    private func comicsInformation(character: Character) {
        view?.showCharactersInformation(character: character)
        
        comicsList = [String: String]()
        
        if let comics = character.comics, let collectionURI = comics.collectionURI {
            comicsList[StringsConstants.ComicsDescription.localized] = collectionURI
        }
        
        if let series = character.series, let collectionURI = series.collectionURI {
            comicsList[StringsConstants.SeriesDescription.localized] = collectionURI
        }
        
        if let stories = character.stories, let collectionURI = stories.collectionURI {
            comicsList[StringsConstants.StoriesDescription.localized] = collectionURI
        }
        
        if let events = character.events, let collectionURI = events.collectionURI {
            comicsList[StringsConstants.EventsDescription.localized] = collectionURI
        }
        
        if let detailElement = comicsList.first {
            interactor?.loadDetail(keyElement: detailElement.key, for: detailElement.value, params: requestParams)
        }
    }
}

extension CharacterDetailsPresenter: DetailInteractOutput {
    func onFetchCharactersSuccess(keyElement: String, characters: ComicsResult?) {
        if (characters?.data?.results ?? []).count > 0 &&
            (characters?.data?.results ?? []).filter({ return $0.thumbnail?.url != nil}).count > 0 {
            characterDetails[keyElement] = characters?.data?.results ?? []
            view?.showCharacterDetails(characterDetails: characterDetails)
        }
        
        comicsList.removeValue(forKey: keyElement)
        
        if let detailElement = comicsList.first {
            interactor?.loadDetail(keyElement: detailElement.key, for: detailElement.value, params: requestParams)
        }
    }
    
    func onFetchCharactersFailure(message: String) {
        print(message)
    }
    
    func onPersistCharactersFailure(message: String) {
        print(message)
    }
    
	
}
