//
//  BasePresenter.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol BasePresenter: class {
    func viewLoad()
    func loadCharacters()
}

