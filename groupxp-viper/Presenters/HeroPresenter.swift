//
//  HeroPresenter.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol ItemSelectDelegate {
    func onHeroSelect()
}

protocol IHeroPresenter: BasePresenter {
    func loadMoreCharacters()
    func favoriteCharacter(character: Character, favorited: Bool)
    func showFavorite(viewController: BaseViewController)
    func showDetail(viewController: BaseViewController, position: Int)
}

class HeroPresenter: IHeroPresenter {
    
    weak var view: IHeroViewController?
    var delegate: ItemSelectDelegate?
    var interactor: IHeroUserCase?
    var wireframe: HeroWireframe?
    var detailWireframe: CharacterDetailWireframe?
    var router: MainRouter?
    var requestParams = RequestParams()
    var charactersResult: CharactersResult?
    
    init(heroView: IHeroViewController, heroInteractor: IHeroUserCase, heroWireframe: HeroWireframe, detailCharacterWireframe: CharacterDetailWireframe) {
        view = heroView
        interactor = heroInteractor
        wireframe = heroWireframe
        detailWireframe = detailCharacterWireframe
    }
    
    func viewLoad() {
        loadCharacters()
    }
    
    func loadCharacters() {
        view?.setViewState(state: .loading)
        requestParams.resetPage()
        interactor?.searchCharacters(with: requestParams)
    }
    
    func loadMoreCharacters() {
        view?.setViewState(state: .loadingMore)
        requestParams.nextPage()
        interactor?.searchCharacters(with: requestParams)
    }
    
    func favoriteCharacter(character: Character, favorited: Bool) {
        interactor?.updateFavorited(character: character, to: favorited)
    }
    
    func showFavorite(viewController: BaseViewController) {
        wireframe?.presentFavoriteController(from: viewController)
    }
    
    func showDetail(viewController: BaseViewController, position: Int) {
        guard let character = charactersResult?.data?.results?[position] else {
            return
        }
        
        detailWireframe?.presentDetailController(from: viewController, character: character)
    }
}

extension HeroPresenter: HeroInteractOutput {
    
    func onFetchCharactersSuccess(characters: CharactersResult?, shouldAppend: Bool) {
        var finalCharacters:[Character] = characters?.data?.results ?? []
        
        if shouldAppend && charactersResult != nil {
            finalCharacters.insert(contentsOf: charactersResult?.data?.results ?? [], at: 0)
            charactersResult?.data?.results = finalCharacters
        } else {
            charactersResult = characters
        }
        
        view?.reloadInformation(characters: finalCharacters)
    }
    
    func onFetchCharactersFailure(message: String) {
        view?.reloadInformation(with: message)
    }
    
    func onPersistCharactersFailure(message: String) {
        view?.reloadInformation(with: message)
    }
}

