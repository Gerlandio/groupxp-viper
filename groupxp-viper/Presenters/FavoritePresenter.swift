//
//  FavoritePresenter.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 04/12/18.
//  Copyright (c) 2018 Gerlandio Da Silva Lucena. All rights reserved.
//

import UIKit

protocol IFavoritePresenter: BasePresenter {
    func favoriteCharacter(character: CharacterCD, favorited: Bool)
}

public class FavoritePresenter: IFavoritePresenter {
	
	var interactor: FavoriteInteractor?
	weak var view: IFavoriteViewController?
    var router: MainRouter?
    
    init(favoriteView: IFavoriteViewController, favoriteInteractor: FavoriteInteractor) {
        view = favoriteView
        interactor = favoriteInteractor
    }
    
    func viewLoad() {
        loadCharacters()
    }
    
    func loadCharacters() {
        view?.setViewState(state: .loading)
        interactor?.fetchLocalCharacters()
    }
    
    func favoriteCharacter(character: CharacterCD, favorited: Bool) {
        interactor?.updateFavorited(character: character, to: favorited)
    }
}

extension FavoritePresenter: FavoriteInteractOutput {
    func onFetchLocalCharacters(characters: [CharacterCD]?) {
        view?.reloadInformation(characters: characters ?? [])
    }
    
    func onPersistCharactersFailure(message: String) {
        view?.reloadInformation(with: message)
    }
    
    func onFetchCharactersFailure(message: String) {
        view?.reloadInformation(with: message)
    }
}
