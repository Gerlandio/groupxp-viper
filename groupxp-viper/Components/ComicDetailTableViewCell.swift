//
//  ComicDetailTableViewCell.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class ComicDetailTableViewCell: UITableViewCell, ReusableView {

    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var collectionView: UICollectionView?
    var datasource: DetailHorizontalDataSource?
    
    var sectionInformation: String? {
        didSet {
            setupSectionView()
        }
    }
    
    var detaiList: [Detail]? {
        didSet {
            setupView()
        }
    }

    func setupView() {
        guard let detaiList = detaiList else {
            return
        }
        datasource = DetailHorizontalDataSource(characters: detaiList)
        collectionView?.dataSource = datasource
        collectionView?.delegate = self
    }
    
    func setupSectionView() {
        self.backgroundColor = UIColor.white
        descriptionLabel?.textColor = UIColor.black
        descriptionLabel?.text = sectionInformation
    }
}

extension ComicDetailTableViewCell: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width * 0.28, height: 150)
    }
}
