//
//  CharacterCollectionViewCell.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
import moa

class CharacterCollectionViewCell: BaseCharacterCollectionViewCell {
    
    var delegate: IHeroDelegate?
    
    var character: Character? {
        didSet{
            setupView()
        }
    }
    
    override func setupView() {
        characterName?.text = character?.name
        setFavorited(favorited: character?.favorited ?? false)
        if image?.moa.url != character?.thumbnail?.url {
            image?.image = nil
            image?.moa.url = character?.thumbnail?.url
        }
    }
    
    @IBAction func favoriteCharacter() {
        character?.favorited = !(character?.favorited ?? false)
        
        guard let character = character else { return }
        
        UIView.animate(withDuration: 0.2, animations: { [weak self] in
            self?.setFavorited(favorited: (character.favorited ?? false))
            self?.favorite?.transform = CGAffineTransform(scaleX: 0.7, y: 0.7)
        }, completion: { finished in
            if finished {
                UIView.animate(withDuration: 0.3, animations: { [weak self] in
                    self?.favorite?.transform = CGAffineTransform.identity
                }, completion: nil)
            }
        })
        
        delegate?.favoritedCharacter(character: character, favorited: (character.favorited ?? false))
    }
}
