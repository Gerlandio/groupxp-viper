//
//  CharacterCollectionViewCell.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
import moa

class FavoriteCharacterCollectionViewCell: BaseCharacterCollectionViewCell {
    
    var delegate: IFavoriteDelegate?
    
    var character: CharacterCD? {
        didSet{
            setupView()
        }
    }
    
    override func setupView() {
        characterName?.text = character?.name
        image?.image = #imageLiteral(resourceName: "placeholder")

        let urlImage = "\(character?.thumbnail?.path ?? "").\(character?.thumbnail?.extensionName ?? "")"
        image?.moa.url = urlImage
    }
    
    @IBAction func favoriteCharacter() {
        guard let character = character else { return }
        
        delegate?.favoritedCharacter(character: character, favorited: false)
    }
}
