//
//  CharacterCollectionViewCell.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class BaseCharacterCollectionViewCell: UICollectionViewCell, ReusableView {
    
    @IBOutlet weak var image: UIImageView?
    @IBOutlet weak var favorite: UIButton?
    @IBOutlet weak var characterName: UILabel?
    
    func setupView() {
    }
    
    func setFavorited(favorited: Bool) {
        let imageButton = favorited ? #imageLiteral(resourceName: "favorite") : #imageLiteral(resourceName: "unfavorite")
        favorite?.setImage(imageButton, for: .normal)
    }
}
