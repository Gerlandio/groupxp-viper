//
//  ImageCollectionViewCell.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
import moa

class ImageCollectionViewCell: UICollectionViewCell, ReusableView {
    @IBOutlet weak var image: UIImageView?
    @IBOutlet weak var detailDescription: UILabel?
    
    var detail: Detail? {
        didSet {
            setupView()
        }
    }
    
    func setupView() {
        image?.image = nil
        image?.moa.url = detail?.thumbnail?.url
        detailDescription?.text = detail?.title
    }
}
