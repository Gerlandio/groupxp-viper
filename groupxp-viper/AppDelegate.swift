//
//  AppDelegate.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
import CoreData

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var mainRouter: MainRouter?
    
    lazy var container: NSPersistentContainer = {
        
        let container = NSPersistentContainer(name: "characters")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error {
                fatalError("Unresolved error, \((error as NSError).userInfo)")
            }
        })
        return container
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        //load window
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.makeKeyAndVisible()
        
        //load main router
        mainRouter = MainRouter(window: window)
        
        //run router
        mainRouter?.loadRootController()
        
        return true
    }
}

