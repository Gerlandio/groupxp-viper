//
//  HeroWireframe.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 08/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol IBaseWireframe: class {
    func createController() -> UIViewController
}

class BaseWireframe: IBaseWireframe {
    
    var mainStoryBoard: UIStoryboard {
        return UIStoryboard(name: "Marvel", bundle: nil)
    }
    
    func createController() -> UIViewController {
        return UIViewController()
    }
}
