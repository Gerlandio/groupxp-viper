//
//  HeroWireframe.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 08/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol IHeroWireframe: IBaseWireframe {
    func presentFavoriteController(from view: BaseViewController)
}

class HeroWireframe: BaseWireframe, IHeroWireframe {
    
    override func createController() -> UIViewController {
        if let controller: FavoriteViewController = mainStoryBoard.instantiate() {
            let favoriteInteractor = FavoriteInteractor()
            let presenter = FavoritePresenter(favoriteView: controller, favoriteInteractor: favoriteInteractor)
            controller.presenter = presenter
            favoriteInteractor.output = presenter
            
            return controller
        }
        
        return UIViewController()
    }

    func presentFavoriteController(from view: BaseViewController) {
        let controller = createController()
            
        view.navigationController?.pushViewController(controller, animated: true)
    }
    

}
