//
//  HeroWireframe.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 08/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol ICharacterDetailWireframe: IBaseWireframe {
    func presentDetailController(from view: BaseViewController, character: Character)
}

class CharacterDetailWireframe: BaseWireframe, ICharacterDetailWireframe {
    var characterItem: Character?
    
    override func createController() -> UIViewController {
        if let controller: CharacterDetailsViewController = mainStoryBoard.instantiate() {
            let interactor = CharacterDetailsInteractor()
            let presenter = CharacterDetailsPresenter(interactor: interactor, view: controller, characterItem: characterItem)
            controller.presenter = presenter
            interactor.output = presenter
            
            return controller
        }
        
        return UIViewController()
    }

    func presentDetailController(from view: BaseViewController, character: Character) {
        characterItem = character
        let controller = createController()
        
        view.navigationController?.pushViewController(controller, animated: true)
    }
    

}
