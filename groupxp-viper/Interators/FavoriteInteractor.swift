//
//  FavoriteInteractor.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 04/12/18.
//  Copyright (c) 2018 Gerlandio Da Silva Lucena. All rights reserved.
//

import UIKit

protocol IFavoriteInteractor: FavoriteCharacterFavorite {
    func fetchLocalCharacters()
}

class FavoriteInteractor: BaseCharacterInteractor {
}

extension FavoriteInteractor: IFavoriteInteractor {
    
    var currentOutput: FavoriteInteractOutput? {
        return output as? FavoriteInteractOutput
    }
    
    func fetchLocalCharacters() {
        do {
            let characters = try localDataManager.fetchLocalFavorites()
            currentOutput?.onFetchLocalCharacters(characters: characters)
        } catch {
          currentOutput?.onFetchCharactersFailure(message: "Não foi possível carregar os dados salvos.")
        }
    }
    
    func updateFavorited(character: CharacterCD, to newValue: Bool) {
        let characterDelete = Character()
        characterDelete.id = Int(character.id)
        do {
            try localDataManager.deleteData(character: characterDelete)
            fetchLocalCharacters()
        } catch {
            currentOutput?.onFetchCharactersFailure(message: "Não foi possível carregar os dados salvos.")
        }
    }
}
