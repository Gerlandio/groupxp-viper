//
//  CharacterDetailsInteractor.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright (c) 2018 Gerlandio Da Silva Lucena. All rights reserved.
//

import UIKit

protocol ICharacterDetailsInteractor: class {
    func loadDetail(keyElement: String, for urlDetail: String, params: RequestParams)
}

class CharacterDetailsInteractor: BaseCharacterInteractor {
    var apiDataManager: DetailApiDataManager = DetailApiDataManager()
}

extension CharacterDetailsInteractor: ICharacterDetailsInteractor {
    
    var currentOutput: DetailInteractOutput? {
        return output as? DetailInteractOutput
    }
    
    func loadDetail(keyElement: String, for urlDetail: String, params: RequestParams) {
        apiDataManager.fetchDetail(keyElement: keyElement, for: urlDetail, params: params) { [weak self] (result) in
            switch result {
            case .success(let key, let comicsResult):
                self?.currentOutput?.onFetchCharactersSuccess(keyElement: key, characters: comicsResult)
            case .error(let message):
                self?.output?.onFetchCharactersFailure(message: message)
            }
        }
    }
}

