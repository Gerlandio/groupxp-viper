//
//  HeroInteractor.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class HeroInteractor: BaseCharacterInteractor {
    var mock = false
    var apiDataManager: HeroApiDataManager = HeroApiDataManager()
}

extension HeroInteractor: IHeroUserCase {
    
    var currentOutput: HeroInteractOutput? {
        return output as? HeroInteractOutput
    }
    
    func searchCharacters(with requestParams: RequestParams) {
        self.apiDataManager.fetchCharacters(params: requestParams, mock: mock) { [weak self] (result) in
            switch result {
                case .success(let charactersResult):
                    if let favoritedList = try? self?.localDataManager.updateFavorite(characters: charactersResult.data?.results ?? []) {
                        var resulCharacter = charactersResult
                        resulCharacter.data?.results = favoritedList
                        self?.currentOutput?.onFetchCharactersSuccess(characters: resulCharacter, shouldAppend: true)
                    } else {
                        self?.currentOutput?.onFetchCharactersSuccess(characters: charactersResult, shouldAppend: true)
                    }
                case .error(let message):
                    self?.output?.onFetchCharactersFailure(message: message)
            }
        }
    }
}
