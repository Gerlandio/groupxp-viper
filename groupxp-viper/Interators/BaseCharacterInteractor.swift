//
//  BaseCharacterInteractor.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 05/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class BaseCharacterInteractor: NSObject {
    weak var output: BaseOutput?
    var localDataManager: CharacterLocalDataManager = CharacterLocalDataManager()
    
    func updateFavorited(character: Character, to newValue: Bool) {
        do {
            if newValue {
                try localDataManager.saveData(character: character)
            } else {
                try localDataManager.deleteData(character: character)
            }
        } catch {
            self.output?.onPersistCharactersFailure(message: error.localizedDescription)
        }
    }
}
