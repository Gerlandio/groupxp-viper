//
//  FirstViewController.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol IHeroViewController: BaseViewProtocol {
    func reloadInformation(characters: [Character])
}

class HeroViewController: BaseViewController, ReusableView {

    @IBOutlet weak var collectionView: UICollectionView?
    var dataSource: HeroHorizontalDataSource?
    
    var presenter: IHeroPresenter?
    let defaultHeight: CGFloat = 200
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.title = StringsConstants.CharactersViewTitle.localized
        presenter?.viewLoad()
    }
    
    @IBAction func showFavoriteController() {
        presenter?.showFavorite(viewController: self)
    }
}

extension HeroViewController: IHeroViewController {
    func reloadInformation(characters: [Character]) {
        if characters.count > 0 {
            setViewState(state: .ready)
            dataSource = HeroHorizontalDataSource(characters: characters, delegateCharacter: self)
            collectionView?.dataSource = dataSource
            collectionView?.delegate = self
        } else {
            setViewState(state: .ready)
            setViewState(state: .error(errorMsg: StringsConstants.EmptyViewMessage.localized, actionTitle: StringsConstants.ActionButtonRetry.localized, action: {[weak self] in
                    self?.setViewState(state: .loading)
                    self?.presenter?.viewLoad()
            }))
        }
    }
    
    func reloadInformation(with message: String) {
        setViewState(state: .error(errorMsg: message, actionTitle: StringsConstants.ActionButtonRetry.localized, action: {[weak self] in
            self?.setViewState(state: .loading)
            self?.presenter?.loadCharacters()
        }))
    }
}

extension HeroViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width * 0.5, height: defaultHeight)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        if (scrollView.bounds.maxY >= scrollView.contentSize.height) {
            setViewState(state: .loadingMore)
            presenter?.loadMoreCharacters()
        }
    }
}

extension HeroViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        presenter?.showDetail(viewController: self, position: indexPath.item)
    }
}

extension HeroViewController: HeroesCharacterFavorite {
    func updateFavorited(character: Character, to newValue: Bool) {
        presenter?.favoriteCharacter(character: character, favorited: newValue)
    }
}

