//
//  CharacterDetailsViewController.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright (c) 2018 Gerlandio Da Silva Lucena. All rights reserved.
//

import UIKit
import moa

protocol ICharacterDetailsViewController: class {
    func showCharactersInformation(character: Character)
    func showCharacterDetails(characterDetails: [String: [Detail]])
}

class CharacterDetailsViewController: BaseViewController, ReusableView {
	var presenter: ICharacterDetailsPresenter?

    @IBOutlet weak var characterImage: UIImageView?
    @IBOutlet weak var descriptionText: UILabel?
    @IBOutlet weak var descriptionLabel: UILabel?
    @IBOutlet weak var tableView: UITableView?
    
    var comicsDataSource: ComicItemsDataSource?
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        presenter?.viewLoad()
    }
    
    func setupView() {
        navigationItem.title = StringsConstants.FavoriteViewTitle.localized
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "backward")
    }
    
    func setupTableView() {
        guard let headerView = tableView?.tableHeaderView else {
            return
        }
        
        let size = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        headerView.frame.size.height = size.height + 40.0
        tableView?.tableFooterView?.isHidden = true
        tableView?.layoutIfNeeded()
        tableView?.estimatedRowHeight = UITableViewAutomaticDimension
        tableView?.sectionHeaderHeight = UITableViewAutomaticDimension
        tableView?.estimatedSectionHeaderHeight = 180.0
        tableView?.rowHeight = 54.0
    }
}

extension CharacterDetailsViewController: ICharacterDetailsViewController {
    func showCharactersInformation(character: Character) {
        setViewState(state: .ready)
        navigationItem.title = character.name ?? StringsConstants.FavoriteViewTitle.localized
        characterImage?.moa.url = character.thumbnail?.url
        descriptionLabel?.text = StringsConstants.DescriptionLabel.localized
        
        descriptionText?.text = character.descriptionValue
        
        if character.descriptionValue == "" || character.descriptionValue == nil {
            descriptionText?.text = StringsConstants.NoDescription.localized
        }
    }
    
    func showCharacterDetails(characterDetails: [String: [Detail]]) {
        setupTableView()
        comicsDataSource = ComicItemsDataSource(comics: characterDetails)
        tableView?.dataSource = comicsDataSource
        tableView?.delegate = comicsDataSource
        tableView?.reloadData()
    }
}
