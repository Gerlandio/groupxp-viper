//
//  BaseViewController.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 04/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

typealias actionButton = () -> Void

enum LoadingState {
    case initial
    case loading
    case loadingMore
    case ready
    case error(errorMsg: String, actionTitle: String, action: actionButton)
}

class BaseViewController: UIViewController {
    
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView?
    var initialView: LoadingFeedbackView?
    var buttonAction: actionButton?
    
    var loadingState: LoadingState = .initial {
        didSet {
            switch loadingState {
            case .initial:
                self.initialView?.isHidden = false
            case .loading:
                self.initialView?.isHidden = false
                self.initialView?.activityIndicator.startAnimating()
            case .loadingMore:
                self.navigationItem.leftBarButtonItem = self.initialView?.loadingMoreIndicator
                self.initialView?.loadingIndicator.startAnimating()
            case .ready:
                self.initialView?.activityIndicator.stopAnimating()
                self.initialView?.loadingIndicator.stopAnimating()
                self.initialView?.isHidden = true
            case .error(let msg, let actionTitle, let action):
                self.initialView?.loadingIndicator.stopAnimating()
                self.initialView?.isHidden = false
                self.buttonAction = action
                self.initialView?.activityIndicator.stopAnimating()
                self.initialView?.labelDescription.text = msg
                self.initialView?.baseButton.isHidden = false
                self.initialView?.baseButton.setTitle(actionTitle, for: .normal)
                self.initialView?.baseButton.removeTarget(self, action: nil, for: .touchUpInside)
                self.initialView?.baseButton.addTarget(self, action: #selector(buttonPressed), for: .touchUpInside)
            }
        }
    }
    
    @objc private func buttonPressed() {
        self.buttonAction?()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initialView = LoadingFeedbackView(parentView: self.view)
    }
    
    
    func setViewState(state: LoadingState) {
        loadingState = state
    }
}
