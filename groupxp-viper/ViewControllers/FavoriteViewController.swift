//
//  FavoriteViewController.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 04/12/18.
//  Copyright (c) 2018 Gerlandio Da Silva Lucena. All rights reserved.
//

import UIKit

protocol IFavoriteViewController: BaseViewProtocol {
    func reloadInformation(characters: [CharacterCD])
}

class FavoriteViewController: BaseViewController, ReusableView {
	
    @IBOutlet weak var collectionView: UICollectionView?
    var dataSource: FavoriteHorizontalDataSource?
    var presenter: IFavoritePresenter?
    
    let defaultHeight: CGFloat = 200
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter?.viewLoad()
    }
    
    func setupView() {
        navigationItem.title = StringsConstants.FavoriteViewTitle.localized
        navigationController?.navigationBar.backIndicatorImage = #imageLiteral(resourceName: "backward")
    }
}

extension FavoriteViewController: IFavoriteViewController {
    func reloadInformation(characters: [CharacterCD]) {
        setViewState(state: .ready)
        dataSource = FavoriteHorizontalDataSource(characters: characters, delegateCharacters: self)
        collectionView?.dataSource = dataSource
        collectionView?.delegate = self
        collectionView?.reloadData()
    }
    
    func reloadInformation(with message: String) {
        loadingState = .error(errorMsg: message, actionTitle: StringsConstants.ActionButtonRetry.localized, action: {[weak self] in
            self?.setViewState(state: .loading)
            self?.presenter?.loadCharacters()
        })
    }
}

extension FavoriteViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.size.width * 0.5, height: defaultHeight)
    }
}

extension FavoriteViewController: FavoriteCharacterFavorite {
    func updateFavorited(character: CharacterCD, to newValue: Bool) {
        self.setViewState(state: .loading)
        presenter?.favoriteCharacter(character: character, favorited: newValue)
    }
}

