//
//  MainRouter.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class MainRouter: UINavigationController {
    
    var window: UIWindow?
    
    convenience init(window: UIWindow?) {
        self.init()
        self.window = window
        setStyle()
    }
    
    private func setStyle() {
        navigationBar.isTranslucent = false
        navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor:UIColor.black]
        navigationBar.tintColor = UIColor.black
        navigationBar.barTintColor = UIColor.white
    }
    
    func loadRootController() {
        viewControllers = [marvelHeroesController()]
        if let window = window { window.rootViewController = self }
    }
}

extension MainRouter {
    
    var mainStoryBoard: UIStoryboard {
        return UIStoryboard(name: "Marvel", bundle: nil)
    }
    
    fileprivate func marvelHeroesController() -> UIViewController {
        
        if let controller: HeroViewController = mainStoryBoard.instantiate() {
            let heroInteractor = HeroInteractor()
            let wireframe = HeroWireframe()
            let detailWireframe = CharacterDetailWireframe()
            let presenter = HeroPresenter(heroView: controller, heroInteractor: heroInteractor, heroWireframe: wireframe, detailCharacterWireframe: detailWireframe)
            controller.presenter = presenter
            heroInteractor.output = presenter
            
            return controller
        }
        
        return UIViewController()
    }
}
