//
//  HeroInteractOutput.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

protocol HeroInteractOutput: BaseOutput {
    func onFetchCharactersSuccess(characters: CharactersResult?, shouldAppend: Bool)
}

protocol DetailInteractOutput: BaseOutput {
    func onFetchCharactersSuccess(keyElement: String, characters: ComicsResult?)
}

protocol BaseOutput: class {
    func onFetchCharactersFailure(message: String)
    func onPersistCharactersFailure(message: String)
}
