//
//  ReusableIdentifier.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

protocol ReusableView: class {}

extension ReusableView {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
}
