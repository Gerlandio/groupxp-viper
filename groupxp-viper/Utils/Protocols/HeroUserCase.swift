//
//  HeroUserCase.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import Foundation

protocol IHeroUserCase: class {
    func searchCharacters(with requestParams: RequestParams)
    func updateFavorited(character: Character, to newValue: Bool)
}

protocol HeroesCharacterFavorite: class {
    func updateFavorited(character: Character, to newValue: Bool)
}

protocol FavoriteCharacterFavorite: class {
    func updateFavorited(character: CharacterCD, to newValue: Bool)
}
