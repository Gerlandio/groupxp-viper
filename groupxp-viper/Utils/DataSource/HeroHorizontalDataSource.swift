//
//  HeroHorizontalDataSource.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol IHeroDelegate: class {
    func favoritedCharacter(character: Character, favorited: Bool)
}

class HeroHorizontalDataSource: BaseHorizontalDataSource<Character> {
    
    var delegate: HeroesCharacterFavorite?
    
    init(characters: [Character], delegateCharacter: HeroesCharacterFavorite) {
        super.init(characters: characters)
        delegate = delegateCharacter
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let character = itemsList?[indexPath.row] else {
            return UICollectionViewCell()
        }
        
        if let characterCell: CharacterCollectionViewCell = collectionView.reusableCell(indexPath: indexPath) {
            characterCell.character = character
            characterCell.delegate = self
            
            return characterCell
        }
        
        return UICollectionViewCell()
    }
}

extension HeroHorizontalDataSource: IHeroDelegate {
    
    func favoritedCharacter(character: Character, favorited: Bool) {
        for char in itemsList ?? [] {
            if char.id == character.id {
                char.favorited = favorited
            }
        }
        
        delegate?.updateFavorited(character: character, to: favorited)
    }
}
