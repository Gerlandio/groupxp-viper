//
//  HeroHorizontalDataSource.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol IFavoriteDelegate: class {
    func favoritedCharacter(character: CharacterCD, favorited: Bool)
}

class FavoriteHorizontalDataSource: BaseHorizontalDataSource<CharacterCD> {
    
    var delegate: FavoriteCharacterFavorite?
    
    init(characters: [CharacterCD], delegateCharacters: FavoriteCharacterFavorite) {
        super.init(characters: characters)
        delegate = delegateCharacters
    }
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let character = itemsList?[indexPath.row] else {
            return UICollectionViewCell()
        }
        
        if let characterCell: FavoriteCharacterCollectionViewCell = collectionView.reusableCell(indexPath: indexPath) {
            characterCell.character = character
            characterCell.delegate = self
            
            return characterCell
        }
        
        return UICollectionViewCell()
    }
}

extension FavoriteHorizontalDataSource: IFavoriteDelegate {
    
    func favoritedCharacter(character: CharacterCD, favorited: Bool) {
        delegate?.updateFavorited(character: character, to: favorited)
    }
}

