//
//  HeroHorizontalDataSource.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

protocol IHeroHorizontalDelegate: class {
    associatedtype characterType
    func favoritedCharacter(character: characterType, favorited: Bool)
}

class BaseHorizontalDataSource<Item>: NSObject, UICollectionViewDataSource {
    var itemsList: [Item]?
    
    init(characters: [Item]) {
        itemsList = characters
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return itemsList?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        return UICollectionViewCell()
    }
}
