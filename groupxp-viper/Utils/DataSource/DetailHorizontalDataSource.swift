//
//  HeroHorizontalDataSource.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class DetailHorizontalDataSource: BaseHorizontalDataSource<Detail> {
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let detail = itemsList?[indexPath.row] else {
            return UICollectionViewCell()
        }
        
        if let characterCell: ImageCollectionViewCell = collectionView.reusableCell(indexPath: indexPath) {
            characterCell.detail = detail
            
            return characterCell
        }
        
        return UICollectionViewCell()
    }
}
