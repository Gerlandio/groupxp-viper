//
//  ComicItemsDataSource.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 09/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

class ComicItemsDataSource: NSObject, UITableViewDataSource, UITableViewDelegate {
    
    var comicList: [String: [Detail]]?
    
    init(comics: [String: [Detail]]?) {
        comicList = comics
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return comicList?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let comicCell: ComicDetailTableViewCell = tableView.dequeue(indexPath: indexPath) {
            
            if let keys = comicList?.keys {
                let key = Array(keys)[indexPath.row]
                if let item = comicList?[key] {
                    comicCell.sectionInformation = key
                    comicCell.detaiList = item
                    return comicCell
                }
            }
        }
        
        return UITableViewCell(style: .default, reuseIdentifier: "")
    }

}
