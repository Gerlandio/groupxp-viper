//
//  HeroApiDataManager.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

enum Result<T> {
    case success(T)
    case error(String)
}

typealias MarvelHeroesCallbackFunction = (Result<CharactersResult>) -> Void

class HeroApiDataManager: NSObject {
    
    private let baseURL = "https://gateway.marvel.com/v1/public/characters"
    private let mockURL = "http://localhost:8080/v1/public/characters"
    
    func fetchCharacters(params: RequestParams, mock: Bool = false, _ callback: @escaping MarvelHeroesCallbackFunction) {
        var finalUrl = "\(baseURL)?\(params.queryString)"
        
        if mock == true {
            finalUrl = "\(mockURL)?\(params.queryString)"
        }
        
        guard let urlRequest = URL(string: finalUrl) else {
            callback(.error(""))
            return
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                DispatchQueue.main.async {
                    callback(.error(error.localizedDescription))
                }
            }
            
            guard let data = data else {
                DispatchQueue.main.async {
                    callback(.error("No response received."))
                }
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(CharactersResult.self, from: data)
                DispatchQueue.main.async {
                    if response.code ?? 500 == 200 {
                        callback(.success(response))
                    } else {
                        callback(.error("No response received."))
                    }
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.error(error.localizedDescription))
                }
            }
        }
        
        task.resume()
    }
}
