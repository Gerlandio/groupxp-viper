//
//  HeroApiDataManager.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 01/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

enum ResultWith<T> {
    case success(String, T)
    case error(String)
}

typealias DetailApiCallbackFunction = (ResultWith<ComicsResult>) -> Void

class DetailApiDataManager: NSObject {
    
    func fetchDetail(keyElement: String, for requestURL: String, params: RequestParams, _ callback: @escaping DetailApiCallbackFunction) {
        
        let finalUrl = "\(requestURL)?\(params.baseQueryString)"
        guard let urlRequest = URL(string: finalUrl) else {
            callback(.error(""))
            return
        }
        
        let task = URLSession.shared.dataTask(with: urlRequest) { (data, response, error) in
            if let error = error {
                callback(.error(error.localizedDescription))
            }
            
            guard let data = data else {
                callback(.error("No response received."))
                return
            }
            
            do {
                let jsonDecoder = JSONDecoder()
                let response = try jsonDecoder.decode(ComicsResult.self, from: data)
                DispatchQueue.main.async {
                    if response.code ?? 500 == 200 {
                        callback(.success(keyElement, response))
                    } else {
                        callback(.error("No response received."))
                    }
                }
            } catch let error {
                DispatchQueue.main.async {
                    callback(.error(error.localizedDescription))
                }
            }
        }
        
        task.resume()
    }
}
