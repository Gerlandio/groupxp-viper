//
//  CharacterLocalDataManager.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 02/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit
import CoreData

class CharacterLocalDataManager: NSObject {

    func saveData(character: Character) throws {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else { return }
        let context = appDelegate.container.viewContext
        
        let characterCd = CharacterCD(context: context)
        characterCd.descriptionValue = character.descriptionValue
        characterCd.id = Int32(character.id ?? 0)
        characterCd.modified = character.modified
        characterCd.name = character.name
        
        let thumbnail = ThumbnailCD(context: context)
        thumbnail.extensionName = character.thumbnail?.extensionName
        thumbnail.path = character.thumbnail?.path
        characterCd.thumbnail = thumbnail
        
        let comicCd = ComicCD(context: context)
        comicCd.available = Int32(character.comics?.available ?? 0)
        comicCd.returned = Int32(character.comics?.returned ?? 0)
        comicCd.collectionURI = character.comics?.collectionURI ?? ""
        
        for item in character.comics?.items ?? [] {
            let itemEntity = ItemsEntity(context: context)
            itemEntity.name = item.name
            itemEntity.resourceURI = item.resourceURI
            comicCd.addToItems(itemEntity)
        }
        
        characterCd.comics = comicCd
        
        let seriesCd = SeriesCD(context: context)
        seriesCd.available = Int32(character.comics?.available ?? 0)
        seriesCd.returned = Int32(character.comics?.returned ?? 0)
        seriesCd.collectionURI = character.comics?.collectionURI ?? ""
        
        for item in character.series?.items ?? [] {
            let itemEntity = ItemsEntity(context: context)
            itemEntity.name = item.name
            itemEntity.resourceURI = item.resourceURI
            seriesCd.addToItems(itemEntity)
        }
        
        characterCd.series = seriesCd
        
        let storiesCd = StoriesCD(context: context)
        storiesCd.available = Int32(character.comics?.available ?? 0)
        storiesCd.returned = Int32(character.comics?.returned ?? 0)
        storiesCd.collectionURI = character.comics?.collectionURI ?? ""
        
        for item in character.stories?.items ?? [] {
            let itemEntity = ItemsEntity(context: context)
            itemEntity.name = item.name
            itemEntity.resourceURI = item.resourceURI
            storiesCd.addToItems(itemEntity)
        }
        
        characterCd.stories = storiesCd
        
        let eventsCd = EventsCD(context: context)
        eventsCd.available = Int32(character.comics?.available ?? 0)
        eventsCd.returned = Int32(character.comics?.returned ?? 0)
        eventsCd.collectionURI = character.comics?.collectionURI ?? ""
        
        for item in character.events?.items ?? [] {
            let itemEntity = ItemsEntity(context: context)
            itemEntity.name = item.name
            itemEntity.resourceURI = item.resourceURI
            eventsCd.addToItems(itemEntity)
        }
        
        characterCd.events = eventsCd
        
        for url in character.urls ?? [] {
            let urlCd = UrlsCD(context: context)
            urlCd.type = url.type
            urlCd.url = url.url
            characterCd.addToUrls(urlCd)
        }
        
        try context.save()
    }
    
    func deleteData(character: Character) throws {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return
        }
        let context = appDelegate.container.viewContext
        
        let fetchRequest: NSFetchRequest = CharacterCD.fetchRequest()
        fetchRequest.predicate = NSPredicate(format: "id = %d", character.id ?? 0)
        let characterCD = try context.fetch(fetchRequest)
        
        if let char = characterCD.first {
            context.delete(char)
            try context.save()
        }
        
    }
    
    func updateFavorite(characters: [Character]) throws -> [Character] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        let context = appDelegate.container.viewContext
        
        var newList = [Character]()
        
        let list = try context.fetch(CharacterCD.fetchRequest())
        
        for character in characters {
            if list.filter({ char in return Int32(character.id ?? 0) == (char as? CharacterCD)?.id ?? 0 }).first != nil {
                let characterNew = character
                characterNew.favorited = true
                newList.append(characterNew)
            } else {
                newList.append(character)
            }
        }
        
        return newList
    }
    
    func fetchLocalFavorites() throws -> [CharacterCD] {
        guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
            return []
        }
        let context = appDelegate.container.viewContext
        
        if let list = try context.fetch(CharacterCD.fetchRequest()) as? [CharacterCD] {
            return list
        }

        return []
    }
}
