//
//  StringsConstantes.swift
//  groupxp-viper
//
//  Created by Gerlandio Da Silva Lucena on 04/12/18.
//  Copyright © 2018 GerlandioLucena. All rights reserved.
//

import UIKit

struct StringsConstants {
    
    static let CharactersViewTitle = "characters.view.title"
    static let FavoriteViewTitle = "favorite.view.title"
    static let ActionButtonRetry = "action.button.retry"
    static let EmptyViewMessage = "empty.view.message"
    static let DescriptionLabel = "description.label"
    static let NoDescription = "description.not_available"
    static let StarringsDescription = "description.starrings"
    static let StoriesDescription = "description.stories"
    static let ComicsDescription = "description.comics"
    static let EventsDescription = "description.events"
    static let SeriesDescription = "description.series"
}
